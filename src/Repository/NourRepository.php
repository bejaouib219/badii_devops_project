<?php

namespace App\Repository;

use App\Entity\Nour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Nour>
 *
 * @method Nour|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nour|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nour[]    findAll()
 * @method Nour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Nour::class);
    }

//    /**
//     * @return Nour[] Returns an array of Nour objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('n')
//            ->andWhere('n.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('n.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Nour
//    {
//        return $this->createQueryBuilder('n')
//            ->andWhere('n.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
