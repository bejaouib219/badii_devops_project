<?php

namespace App\Repository;

use App\Entity\Badii;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Badii>
 *
 * @method Badii|null find($id, $lockMode = null, $lockVersion = null)
 * @method Badii|null findOneBy(array $criteria, array $orderBy = null)
 * @method Badii[]    findAll()
 * @method Badii[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BadiiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Badii::class);
    }

//    /**
//     * @return Badii[] Returns an array of Badii objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Badii
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
